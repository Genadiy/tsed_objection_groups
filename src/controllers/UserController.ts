import { Controller, Get } from '@tsed/common';
import { Groups, Property, Returns } from '@tsed/schema';

@Groups<User1>({
  // g1: ['id'],   <- there are no groups
})
export class User1 {
  @Property() id: Number;
}

@Groups<User2>({
  g2: ['id'],
})
export class User2 {
  // @Groups('g22')
  @Property()
  id: Number;
}
export class TwoUsers {
  @Property() one: User1;
  @Property() two: User2;
}

@Controller('/2-users')
export class UsersController {
  @Get('/')
  @Returns(200, TwoUsers)
  // Result does not depend on Groups in controller
  // @(Returns(200, TwoUsers).Groups('g2'))
  async get(): Promise<Boolean> {
    return true;
  }
}

// Response description in Swagger:
// {
//   "one": {
//     "id": 0
//   },
//   "two": {}   <- second user without properties
// }
