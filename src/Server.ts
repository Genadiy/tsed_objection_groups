import '@tsed/ajv';
import '@tsed/swagger';
import '@tsed/platform-express';
import { Configuration, Inject } from '@tsed/di';
import { PlatformApplication } from '@tsed/common';
import Express from 'express';
import methodOverride from 'method-override';

export const rootDir = __dirname;

@Configuration({
  rootDir,
  acceptMimes: ['application/json'],
  httpPort: process.env.API_PORT,
  httpsPort: false,
  responseFilters: [],
  mount: {
    '/api/v1': [`${rootDir}/controllers/**/*.ts`],
  },

  swagger: [
    {
      path: '/v3-docs',
      specVersion: '3.0.1',
    },
  ],
})
export class Server {
  @Inject()
  app: PlatformApplication;

  @Configuration()
  settings: Configuration;

  $beforeRoutesInit(): void {
    // Quick changes for sqlite  + knex, Ts.ED config  skipped

    this.app
      .use(methodOverride())
      .use(Express.json())
      .use(
        Express.urlencoded({
          extended: true,
        })
      );
  }
}
